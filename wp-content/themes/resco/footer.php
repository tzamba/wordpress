<?php
/**
 * The template for displaying the footer
 */
?>

    <footer class="wrapper wrapper-gray">
        <div class="container">
            <div class="container-line-wrapper container-line-white">
                <div class="container-line container-line-bullet container-line-top">
                    <span></span>
                </div>
                <div class="container-line-content">
                    <?php if ( has_nav_menu( 'primary' ) ) : ?>
                    <?php
                        wp_nav_menu(array(
                            'theme_location' => 'primary',
                            'menu_class' => 'footer-menu',
                        ));
                    ?>
                    <?php endif; ?>
                </div>
                <div class="container-line container-line-bullet container-line-bottom">
                    <span></span>
                </div>
            </div>

            <div class="container-content flex flex-column flex-center">
                <?php get_sidebar( 'newsletter' ); ?>
            </div>
        </div>
    </footer>

<?php wp_footer(); ?>
</body>
</html>
