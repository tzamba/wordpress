<?php
/**
 * Twenty Sixteen Customizer functionality
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

/**
 * Sets up the WordPress core custom header and custom background features.
 *
 * @since Twenty Sixteen 1.0
 *
 * @see resco_header_style()
 */
function resco_custom_header_and_background() {

	/**
	 * Filter the arguments used when adding 'custom-header' support in Twenty Sixteen.
	 *
	 * @since Twenty Sixteen 1.0
	 *
	 * @param array $args {
	 *     An array of custom-header support arguments.
	 *
	 *     @type string $default-text-color Default color of the header text.
	 *     @type int      $width            Width in pixels of the custom header image. Default 1200.
	 *     @type int      $height           Height in pixels of the custom header image. Default 280.
	 *     @type bool     $flex-height      Whether to allow flexible-height header images. Default true.
	 *     @type callable $wp-head-callback Callback function used to style the header image and text
	 *                                      displayed on the blog.
	 * }
	 */
	add_theme_support( 'custom-header', apply_filters( 'resco_custom_header_args', array(
		'default-text-color'     => '#38283b', // dark purple
		'width'                  => 2400,
		'height'                 => 1400,
		'flex-height'            => true,
		'wp-head-callback'       => 'resco_header_style',
	) ) );
}
add_action( 'after_setup_theme', 'resco_custom_header_and_background' );

if ( ! function_exists( 'resco_header_style' ) ) :
/**
 * Styles the header text displayed on the site.
 *
 * Create your own resco_header_style() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 *
 * @see resco_custom_header_and_background().
 */
function resco_header_style() {
	// If the header text option is untouched, let's bail.
	if ( display_header_text() ) {
		return;
	}

	// If the header text has been hidden.
	?>
	<style type="text/css" id="resco-header-css">
		.site-branding {
			margin: 0 auto 0 0;
		}

		.site-branding .site-title,
		.site-description {
			clip: rect(1px, 1px, 1px, 1px);
			position: absolute;
		}
	</style>
	<?php
}
endif; // resco_header_style

/**
 * Render the site title for the selective refresh partial.
 *
 * @since Twenty Sixteen 1.2
 * @see resco_customize_register()
 *
 * @return void
 */
function resco_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since Twenty Sixteen 1.2
 * @see resco_customize_register()
 *
 * @return void
 */
function resco_customize_partial_blogdescription() {
	bloginfo( 'description' );
}