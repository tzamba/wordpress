(function ($) {

  $(document).ready(function () {

    setTimeout(function () {
      $('body').addClass('loaded');
    }, 300);

    function setBackground(swiper) {
      var $activeSlide = $(swiper.slides[swiper.activeIndex]);
      var style = $activeSlide.attr('data-style');
      $('.wrapper-swiper').attr('style', style);
    }

    var mySwiper = new Swiper('.swiper-container', {
      initialSlide: 0,

      pagination: '.swiper-pagination',
      paginationClickable: true,

      nextButton: '.swiper-button-next',
      prevButton: '.swiper-button-prev',
      onInit: setBackground,
      onSlideChangeEnd: setBackground
    })

    $('.primary-menu-toggle').on('click', function () {
      $('.primary-menu-container').slideToggle();
      $('#nav-icon4').toggleClass('open');
    })
  });

})(jQuery);
