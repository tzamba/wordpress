<?php
/**
 * Template Name: Articles
 */

get_header(); ?>

<div class="wrapper">
    <div class="container">
        <div class="container-line-wrapper container-line-burgundy">
            <div class="container-line container-line-top container-line-small">
                <span></span>
            </div>
            <div class="container-line-content text-darkblue">
                <h1><?php echo "Aktuality"; ?></h1>
            </div>
            <div class="container-line container-line-bottom">
                <span></span>
            </div>
        </div>

        <div class="container-content">
            <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $args = array('posts_per_page' => 1, 'paged' => $paged);
            $wp_query= new WP_Query($args);

            while ( $wp_query->have_posts() ) :
            $wp_query->the_post();
            $id    = $wp_query->post->ID;
            $title = get_the_title($id);
            ?>
            <article class="article article-horizontal">
                <?php $thumb = get_the_post_thumbnail($id, 'medium'); ?>
                <?php if (!empty($thumb)) : ?>
                <div class="article-image">
                    <?php echo get_the_post_thumbnail($id, 'medium'); ?>
                </div>
                <?php endif; ?>
                <header class="header">
                    <h2 class="title"><a href="<?php the_permalink(); ?>" class="text-burgundy"><?php echo $title; ?></a></h2>
                    <small><?php echo strtolower(get_the_date('d. F Y')); ?></small>
                    <?php the_excerpt(); ?>
                </header>
            </article>

            <br>
            <br>
            <br>

            <?php
            endwhile;
            // wp_pagenavi('<div class="pager">', '</div>');

        the_posts_pagination( array(
        'prev_text'          => __( 'Previous page', 'resco' ),
        'next_text'          => __( 'Next page', 'resco' ),
        'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'resco' ) . ' </span>',
        ) );

        wp_reset_query();
        ?>
    </div>
</div>

<?php get_footer(); ?>