<?php
/**
 * The template for displaying pages
 */

get_header(); ?>

<div class="wrapper">
	<div class="container">
		<div class="container-line-wrapper container-line-burgundy">
			<div class="container-line container-line-top container-line-small">
				<span></span>
			</div>
			<div class="container-line-content text-darkblue">
				<h1><?php echo get_the_title(); ?></h1>
			</div>
			<div class="container-line container-line-bottom">
				<span></span>
			</div>
		</div>

		<div class="container-content">
			<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content', 'page' );
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}
		endwhile;
		?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
