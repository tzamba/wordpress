<?php
/**
 * The template for the newsletter form
 */
?>

<div>
    <h3 class="text-white">
        Newsletter
        <br><small class="text-burgundy">Prihláste sa na odosielanie newsletter-a</small>
        <br>
        <br>
    </h3>
    <?php echo do_shortcode('[ninja_form id=1]'); ?>
</div>
