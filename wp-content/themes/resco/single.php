<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<div class="container">
	<div class="container-line-wrapper container-line-burgundy">
		<div class="container-line container-line-top container-line-small">
			<span></span>
		</div>
		<div class="container-line-content text-center text-darkblue">
			<h1><?php echo get_the_title(); ?></h1>
		</div>
		<div class="container-line container-line-bottom">
			<span></span>
		</div>
	</div>

	<div class="container-content">
		<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content', 'single' );
			if ( comments_open() || get_comments_number() ) {
				// comments_template();
			}
		endwhile;
		?>
	</div>
</div>

<?php get_footer(); ?>