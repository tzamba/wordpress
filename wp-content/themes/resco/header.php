<?php
/**
 * The template for displaying the header
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<!--FONTAWESOME-->
	<script src="https://use.fontawesome.com/3e6c4c1e5c.js"></script>
	<!--SWIPER-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.min.js"></script>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<?php
		$imgPage = get_header_image();
		if ( has_post_thumbnail() ) {
			$imgPage = wp_get_attachment_url(get_post_thumbnail_id());
		}
	?>

	<div class="wrapper">
		<header class="flex flex-column" style="background-image: url('<?php echo $imgPage; unset($imgPage); ?>');">
			<div class="container">
				<div class="flex flex-full">
					<a class="site-title" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
					<div class="primary-menu-toggle">
						<div id="nav-icon4">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
					<div class="flex-grow primary-menu-container">
						<?php if ( has_nav_menu( 'primary' ) ) : ?>
						<?php
							wp_nav_menu( array(
								'container' => false,
                                'theme_location' => 'primary',
                                'menu_class'     => 'primary-menu',
						) );
						?>
						<?php endif; ?>
						<ul class="lang-menu">
							<?php pll_the_languages(array('display_names_as'=> 'slug')); ?>
						</ul>
					</div>
				</div>
			</div>

			<div class="container flex-grow">
				<div class="container-line-wrapper">
					<div class="container-line container-line-top">
						<span></span>
					</div>
					<div class="container-line-content">
						<?php the_custom_logo(); ?>
					</div>
					<div class="container-line container-line-bottom">
						<span></span>
					</div>
				</div>
				<?php if (is_front_page()) : ?>
				<div class="container-content flex flex-align">
					<div id="intro">
						QUIDIS VENDUNDI UR<br>
						LICIIS ANIHILLA DOLU PRE
					</div>
				</div>
				<?php endif; ?>
			</div>
		</header>
	</div>