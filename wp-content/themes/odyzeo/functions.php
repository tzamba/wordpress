<?php

/**
 * Enqueue scripts and styles.
 */
function odyzeo_scripts() {
    wp_enqueue_style('odyzeo-style', get_stylesheet_uri(), array(), '1.0.0', 'all');
    wp_enqueue_script('odyzeo-app', get_theme_file_uri('./app.js'), array('jquery'), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'odyzeo_scripts');
