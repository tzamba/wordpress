<article id="post-<?php the_ID(); ?>" <?php post_class( 'memento' ); ?>>

    <div class="vedome__left">
        <div class="vedome__left-top"></div>
	    <h1 class="vedome__title">
            <?php the_title(); ?>
        </h1>
    </div>

    <div class="vedome__right">
	    <div class="vedome__content">
		    <?php the_content(); ?>
        </div>
    </div>

</article>
