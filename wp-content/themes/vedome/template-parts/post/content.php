<article id="post-<?php the_ID(); ?>" <?php post_class( 'vedome__content' ); ?>>

	<?php the_content(); ?>

</article>
