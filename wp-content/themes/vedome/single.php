<?php
/**
 * Template for single post
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

    <div class="memento">
        <div class="vedome__left">
            <div class="vedome__left-top"></div>
            <div>
                <h1 class="vedome__title">
					<?php the_title(); ?>
                </h1>

				<?php

				if ( 'post' === get_post_type() ) {
					echo '<div class="vedome__meta">';
					if ( is_single() ) {
						vedome_posted_on();
					} else {
						echo vedome_time_link();
						vedome_edit_link();
					};
					echo '</div>';
				}

				if ( is_single() ) {
					vedome_entry_footer();
				}

				?>
            </div>
        </div>

        <div class="vedome__right">

			<?php

			get_template_part( 'template-parts/post/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			the_post_navigation( array(
				'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'twentyseventeen' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">SVGLEFTZZZ</span>%title</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'twentyseventeen' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'twentyseventeen' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">SVGRIGHTZZZ</span></span>',
			) );

			?>

        </div>
    </div>

<?php endwhile; ?>

<?php get_footer();

