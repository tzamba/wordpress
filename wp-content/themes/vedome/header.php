<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="wrapper">
    <div class="container">

		<?php if ( has_nav_menu( 'primary' ) ) :
			wp_nav_menu( array(
				'container'      => false,
				'theme_location' => 'primary',
				'menu_class'     => 'menu-primary',
			) );
		endif; ?>

    </div>
</div>