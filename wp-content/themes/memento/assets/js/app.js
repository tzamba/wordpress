import Velocity from 'velocity-animate'
import 'velocity-animate/velocity.ui'

/**
 * http://easings.net/
 */
window.$ = jQuery

$(document).ready(() => {
  const memento = {
    memento: document.querySelector('.memento'),
    left: document.querySelector('.memento__left'),
    leftTop: document.querySelector('.memento__left-top'),
    right: document.querySelector('.memento__right'),
  }

  const topTime = 700

  const mySequence = [
    { e: memento.left, p: { opacity: 1 }, o: { duration: 200, easing: 'easeInQuart' } },
    { e: memento.left, p: { left: '0%' }, o: { duration: 400, easing: 'easeOutQuart', delay: 300, } },
    { e: memento.leftTop, p: { height: '100%' }, o: { duration: topTime, easing: 'easeInOutQuart' } },
    { e: memento.left, p: { color: '#fff' }, o: { duration: topTime, sequenceQueue: false } },
  ]

  setTimeout(() => {
    Velocity.RunSequence(mySequence)

    setTimeout(() => {
      console.log('after animation')
      memento.right.classList.add('memento__right--visible')
      memento.left.classList.add('memento__left--visible')
      memento.memento.classList.add('memento--visible')

      Velocity(memento.right, { opacity: 1 }, { duration: 400 })
    }, 1600)

  }, 400)
})

