<?php
/**
 * The front page template file
 */
get_header(); ?>

<?php
get_template_part( 'template-parts/page/content', 'front-page' );
?>

<?php get_footer();
