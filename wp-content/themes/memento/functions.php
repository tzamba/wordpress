<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {

	wp_enqueue_style('odyzeo-style', get_stylesheet_uri(), array(), '1.0.0', 'all');
	wp_enqueue_script( 'app', get_theme_file_uri( '/app.js' ), array('jquery'), '1.0.0' );
}


/**
 * Custom template tags for this theme.
 */
require get_theme_file_path( '/inc/template-tags.php' );
