<article id="post-<?php the_ID(); ?>" <?php post_class( 'memento' ); ?>>

    <div class="memento__left">
        <div class="memento__left-top"></div>
	    <h1 class="memento__title">
            <?php the_title(); ?>
        </h1>
    </div>

    <div class="memento__right">
	    <div class="memento__content">
		    <?php the_content(); ?>
        </div>
    </div>

</article>
