<div class="memento">

    <div class="memento__left">
        <div class="memento__left-top"></div>
        <h1 class="memento__title">
            Memento
        </h1>
    </div>

    <div class="memento__right">
        <div class="memento__content">
            <h1>Welcome</h1>
            <h2>How are you?</h2>
            <h3>I hope you are fine</h3>
            <h4>I actually think you are well</h4>
            <h5>Not just well, but - better then 90% of the world - WELL</h5>
            <h6>Think a about it for a while</h6>
        </div>
    </div>

</div>
