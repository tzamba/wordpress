<article id="post-<?php the_ID(); ?>" <?php post_class( 'memento__content' ); ?>>

	<?php the_content(); ?>

</article>
