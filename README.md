#output - wordpress boilerplate

## Installation and Development
1. Create new empty db
2. Copy `wp-config-sample.php` to `wp-config.php` and change DB info + change
```
define('WP_HOME','http://localhost/wordpress');
define('WP_SITEURL','http://localhost/wordpress');
```
3. Go to [http://localhost/wordpress](http://localhost/wordpress) and fill the info

## Frontend Development
0. `cd wp-content/themes/your_theme`
1. Setup gulpfile.js and change
	`proxy: 'localhost/wordpress`
2. Setup package.json
3. Update `/assets/less/style.less` 
	`Theme Name: Your theme name`
3. Run `yarn`
4. Run `yarn start`
5. Wait ... Page will automatically open in your browser